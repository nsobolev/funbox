import Basket from './modules/basket';

const basket = new Basket({
  countSelector: '.basket__count',
  productSelector: '.products__item',
  productDisabledClass: '.products__item_view_disabled'
});