export default class Basket {
  constructor(initProps) {
    const defaultProps = {
      countSelector: '',
      productSelector: '',
      productDisabledSelector: '',
    };

    const props = { ...defaultProps, ...initProps };
    
    const products = document.querySelectorAll(`${props.productSelector}:not(${props.productDisabledClass})`);
    const basketCount = document.querySelector(props.countSelector);

    if (!products || !basketCount) return false;

    this.products = [ ...products ];
    this.basketCount = basketCount;  
    this.selectedProducts = [];

    console.log(this.products);

    this.init();
  }
  
  init() {
    this.products.forEach(product => {
      product.addEventListener('click', (evt) => {
        this.ProductClickHandler(evt);
      });
    });
  }

  ProductClickHandler(evt) {
    const item = evt.currentTarget;
    const productName = item.dataset.productName;
    
    if (productName) {
      const selectedProductIndex = this.selectedProducts.indexOf(productName);
      if (selectedProductIndex > -1) {
        this.removeProduct(selectedProductIndex);
      } else {
        this.addProduct(productName);
        
        item.addEventListener('mouseleave', function() {
            item.classList.add(`products__item_type_${productName}_active`);
        });
      }

      this.renderCount();
    }
  }

  addProduct(name) {
    this.selectedProducts.push(name);
  }

  removeProduct(index) {
    this.selectedProducts.splice(index, 1);
  }

  renderCount() {
    this.basketCount.textContent = this.selectedProducts.length;
  }
}
