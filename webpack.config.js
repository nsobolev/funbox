const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LessPluginGlob = require('less-plugin-glob');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const fs = require('fs');

const generateHtmlPlugins = (templateDir) => {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir)); // Вернет массив файлов в папке templateDir
  return templateFiles.map(item => {
    const parts = item.split('.');
    const file = {
      name: parts[0],
      ext: parts[1]
    }
    return new HtmlWebpackPlugin({
      filetype: 'pug',
      inject: false,
      hash: true,
      template: path.resolve(__dirname, `${templateDir}/${file.name}.${file.ext}`), 
      filename: `${file.name}.html`
    })
  });
};

const paths = {
  source: {
    js: './src/js/index.js',
    html: './src/html/views',
    less: './src/css/style.less'
  },
  production: {
    js: './js/index.js',
    css: './css/style.css',
    copy: [
      { 
        from: './src/images',
        to: 'images'
      },
      {
        from: './src/favicon',
        to: 'favicon'
      },
      {
        from: './src/fonts',
        to: 'fonts'
      }
    ] 
  }
}; 

module.exports = {
  entry: [ 
    paths.source.js,
    paths.source.less
    ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: paths.production.js
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src/js'),
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.less$/,
        include: path.resolve(__dirname, 'src/css'),
        use: [
            'css-hot-loader',
            MiniCssExtractPlugin.loader,
            { 
              loader: 'css-loader', 
              options: {
                sourceMap: true,
                url: false
              }
            },
            {
              loader: 'less-loader', 
              options: { 
                paths: [ path.resolve(path.join(__dirname, 'dist')) ], 
                plugins: [ LessPluginGlob ],
                sourceMap: true
               }
           }
         ]
      },
      {
        test: /\.pug$/, 
        loader: [
          'raw-loader', 
          'pug-html-loader'
        ],
      },
      {
        test: /\.svg$/,
        use: [
          { 
            loader: 'svg-sprite-loader',
            options: {
              spriteFilename: svgPath => `sprite${svgPath.substr(-4)}`
             }
          },
          'svgo-loader'
        ]
      }
    ]
  },
  plugins: [ 
    new MiniCssExtractPlugin({
        filename: paths.production.css,
        //allChunks: true
      }),
    new CopyWebpackPlugin(paths.production.copy)
  ].concat(generateHtmlPlugins(paths.source.html))
}
